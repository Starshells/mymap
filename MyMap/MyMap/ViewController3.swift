//
//  ViewController3.swift
//  MyMap
//
//  Created by Students on 16/05/2018.
//  Copyright © 2018 students. All rights reserved.
//

import UIKit
import MapKit

class ViewController3: UIViewController, UITextFieldDelegate {
    
    var delegate: VC3Delegate?

    @IBOutlet weak var latitudeTexBox: UITextField!
    @IBOutlet weak var longitudeTextBox: UITextField!
    
    @IBOutlet weak var textboxNoticeLabel: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        latitudeTexBox.delegate = self
        longitudeTextBox.delegate = self
    }
    
    @IBAction func addButtonAction(_ sender: Any)
    {
        
        if  let latitude = Double(latitudeTexBox.text!),
            let longitude = Double(longitudeTextBox.text!)
        {
            let loc = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            
            if CLLocationCoordinate2DIsValid(loc)
            {
                self.textboxNoticeLabel.isHidden = true
                delegate?.addNewMapPoint(coordinates:loc)
            }
            else
            {
                self.textboxNoticeLabel.textColor = .red
                self.textboxNoticeLabel.text = "Invalid coordinates"
                self.textboxNoticeLabel.isHidden = false
            }
        }
        else
        {
            self.textboxNoticeLabel.textColor = .blue
            self.textboxNoticeLabel.text = "Enter latitude and longitude"
            self.textboxNoticeLabel.isHidden = false
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if (textField.text == "" && string == "-") || (textField.text == "-" && string == "")
        {
            return true
        }
        
        if textField.text != "" || string != ""
        {
            let res = (textField.text ?? "") + string
            return Double(res) != nil
        }
        
        return true
    }
}

protocol VC3Delegate: class
{
    func addNewMapPoint(coordinates:CLLocationCoordinate2D)
}
