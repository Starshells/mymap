//
//  ViewController.swift
//  MyMap
//
//  Created by students on 12/04/2018.
//  Copyright © 2018 students. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, VC2Delegate, VC3Delegate, CLLocationManagerDelegate, MKMapViewDelegate
{
    
    var myLocation: CLLocationCoordinate2D = CLLocationCoordinate2D()
    let locationManager = CLLocationManager()
    
    @IBOutlet var mapView: MKMapView!
    
    @IBAction func login(_ sender: UIButton) {
        
        performSegue(withIdentifier: "Segue2", sender: self)}
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "Segue", let vc = segue.destination as? ViewController2{
            vc.delegate = self
        }
    }
    
    func ChangeAnnotation(_ sender: Bool) {
        let defaults = UserDefaults.standard
        defaults.set(sender, forKey: "SwitchOn")
        annoVisibility()
    }
    
    func annoVisibility (){
        let defaults = UserDefaults.standard
        let visible = defaults.bool(forKey: "SwitchOn")
        
        for annotation in mapView.annotations {
            if (annotation.subtitle == "") {
                mapView.view(for: annotation)?.isHidden = visible
            }
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        self.myLocation = mapView.centerCoordinate
        
        var firstName : String = ""
        var firstDesc : String = ""
        var firstLat : String = ""
        var firstLong : String = ""
        
        var secondName : String = ""
        var secondDesc : String = ""
        var secondLat : String = ""
        var secondLong : String = ""
        
        var thirdName : String = ""
        var thirdDesc : String = ""
        var thirdLat : String = ""
        var thirdLong : String = ""

        
        if let path = Bundle.main.path(forResource: "MapData", ofType: "plist") {
            let array = NSArray(contentsOfFile: path)
           // debugPrint(path)
            if let dict = array {
                let value = dict[0] as! NSDictionary
                firstName = value["Name"] as! String
                firstDesc = value["Description"] as! String
                firstLat = value["Latitude"] as! String
                firstLong = value["Longitude"] as! String
               //debugPrint(firstName)
                
            }
            
            if let dict = array {
                let value = dict[1] as! NSDictionary
                secondName = value["Name"] as! String
                secondDesc = value["Description"] as! String
                secondLat = value["Latitude"] as! String
                secondLong = value["Longitude"] as! String
         //       debugPrint(secondName)
                
            }
            
            if let dict = array {
                let value = dict[2] as! NSDictionary
                thirdName = value["Name"] as! String
                thirdDesc = value["Description"] as! String
                thirdLat = value["Latitude"] as! String
                thirdLong = value["Longitude"] as! String
          //     debugPrint(thirdLong)
                
            }
        }
        
        
        let firstPoint = MKPointAnnotation()
        firstPoint.coordinate = CLLocationCoordinate2D(latitude: Double(firstLat)!, longitude: Double(firstLong)!)
        firstPoint.title = firstName
        firstPoint.subtitle = firstDesc
        
        
        mapView.addAnnotation(firstPoint)
        
        let secondPoint = MKPointAnnotation()
        secondPoint.coordinate = CLLocationCoordinate2D(latitude: Double(secondLat)!, longitude: Double(secondLong)!)
        secondPoint.title = secondName
        secondPoint.subtitle = secondDesc
        
        
        mapView.addAnnotation(secondPoint)
        
        let thirdPoint = MKPointAnnotation()
        thirdPoint.coordinate = CLLocationCoordinate2D(latitude: Double(thirdLat)!, longitude: Double(thirdLong)!)
        thirdPoint.title = thirdName
        thirdPoint.subtitle = thirdDesc
        
        mapView.addAnnotation(thirdPoint)
        
       DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
          self.annoVisibility()
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        self.myLocation = locations[0].coordinate
        let region = MKCoordinateRegion(center: locations[0].coordinate, span: mapView.region.span)
        
        mapView.setRegion(region, animated: true)
    }
    
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let request = MKDirectionsRequest()
        
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: self.myLocation, addressDictionary: nil))
        //forCurrentLocation()
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: (view.annotation?.coordinate)!))
        request.requestsAlternateRoutes = true
        request.requestsAlternateRoutes = true
        request.transportType = .walking
        let directions = MKDirections(request: request)
        directions.calculate { [unowned self] response, error in
            guard let unwrappedResponse = response else { return }
            
            for route in unwrappedResponse.routes {
                self.mapView.add(route.polyline, level: MKOverlayLevel.aboveRoads)
                self.mapView.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
                return
            }
        }
        
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let polyLine = overlay
        let polyLineRenderer = MKPolylineRenderer(overlay: polyLine)
        polyLineRenderer.strokeColor = .red
        polyLineRenderer.lineWidth = 2.0
        return polyLineRenderer
    }
    
    func addMapPoint(title:String, latitude:Double, longitude:Double, description:String?)
    {
        let mapPoint = MKPointAnnotation();
        
        mapPoint.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        mapPoint.title = title
        
        if description != nil
        {
            mapPoint.subtitle = description
        }
        
        mapView.addAnnotation(mapPoint)
    }
    
    func addNewMapPoint(coordinates: CLLocationCoordinate2D)
    {
        let title = String(coordinates.latitude)+"'"+String(coordinates.longitude)
        self.addMapPoint(title: title, latitude: coordinates.latitude, longitude: coordinates.longitude, description: "")
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
